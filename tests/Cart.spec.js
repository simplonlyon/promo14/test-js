import { Cart } from "../src/Cart";
import { Product } from "../src/Product";

describe('class Cart', () => {

    it('should instantiate an empty cart', () => {
        const cart = new Cart();

        expect(cart).toBeDefined();
        expect(cart.products.length).toBe(0);
    });

    it('should add product in cart on addToCart', () => {
        const cart = new Cart();
        const product = new Product('Test', 5);

        cart.addToCart(product);
        expect(cart.products).toContain(product);

    });

    it('should sum product prices on getTotal', () => {
        const cart = new Cart();

        cart.products = [
            new Product('Test', 10),
            new Product('Test', 5),
            new Product('Test', 2)
        ];

        expect(cart.getTotalPrice()).toBe(17);
    });


})