import { Counter } from "../src/Counter";


describe('class Counter', () => {

    let instance;

    beforeEach(() => {
        instance = new Counter();
    });

    it('Should create a new instance', () => {


        expect(instance).toBeDefined();
        expect(instance.value).toBe(0);
        
    });

    it('Should increment counter on increment call', () => {

        instance.increment();

        expect(instance.value).toBe(1);

    });

    it('Should decrement counter on decrement call', () => {

        instance.decrement();

        expect(instance.value).toBe(-1);

    });

    it('Should reset value to zero on reset call', () => {

        instance.value = 3;
        instance.reset();

        expect(instance.value).toBe(0);

    });
})