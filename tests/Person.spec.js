import { Person } from "../src/Person";

describe('class Person', () => {


    it('should create an instance of Person', () => {
        let person = new Person('Nom', 10);
        expect(person).toBeDefined();
        expect(person.name).toBe('Nom');
        expect(person.age).toBe(10);
    });

    it('should return year of birth', () => {
        //Vous pouvez ignorer ça pour le moment, mais en gros c'est pour faire en sorte de contrôler la valeur renvoyée par le getFullYear des Date en leur donnant une valeur fixe
        Date = jest.fn().mockImplementation(() => ({getFullYear:() => 2021}));
        

        let person = new Person('Nom', 10);

        let result = person.getBirthYear();

        expect(result).toBe(2011);


    });

    it('should generate HTML element for a Person', () => {

        let person = new Person('Test', 10);
        let html = person.toHTML();

        expect(html).toBeInstanceOf(HTMLElement);
        expect(html.nodeName).toBe('P'); //Optionnel selon si c'est un important que ça soit un paragraphe ou pas
        expect(html.textContent).toBe('Name: Test, age: 10');
    });

});