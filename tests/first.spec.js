import { add } from "../src/index";

describe('Example test without class', () => {

    it('Should do assert that true is true', () => {
        expect(true).toBe(true);
    });

    it('Should add two numbers', () => {

        expect(add(4,2)).toBe(6);
    });
    it('Should add two numbers even if string given', () => {

        expect(add('4',2)).toBe(6);
    });
    it('Should return Not A Number if word given', () => {

        expect(add('bloup',2)).toBe(NaN);
    });

})