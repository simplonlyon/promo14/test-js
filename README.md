# Tests
Projet utilisant Jest pour commencer à tester nos programmes. L'idée des tests va être d'automatisé les tests faits sur les différentes parties de notre code afin que ceux ci se relancent automatiquement et ainsi améliorer la stabilité du code et sa qualité.

## How To Use
1. Cloner le projet
2. `npm install` pour installer les dépendances
3. `npm test` pour lancer les tests en mode watch
   
## Pour configurer un projet classique pour y faire des tests 
1. Installer les librairies nécessaire pour les tests `npm i jest @types/jest babel-jest @babel/core @babel/preset-env -D` (Jest nous permettra de faire les tests, @types/jest c'est pour l'autocomplétion dans vscode, et tous les babel c'est pour faire qu'on puisse utiliser les import {} from '...' dans nos tests)
2. Créer un fichier `babel.config.js` sur le modèle de [celui ci](babel.config.js)
3. Modifier le package.json pour mettre dans la parties `scripts` ceci : `"test":"jest --watch"`
4. Créer un dossier tests et dedans créer des `fichier.spec.js`
5. Lancer `npm test` pour lancer vos tests