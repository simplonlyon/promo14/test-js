

export class Product {
    
    label;
    price;
    /**
     * 
     * @param {string} label 
     * @param {number} price 
     */
    constructor(label, price) {
        this.label = label;
        this.price = price;
    }
}