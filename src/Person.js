

export class Person {
    name;
    age;
    constructor(name, age) {
        this.name =name;
        this.age = age;
    }

    getBirthYear() {
        let actualYear = new Date().getFullYear();
        return actualYear-this.age;
    }

    toHTML() {
        const p = document.createElement('p');
        p.textContent = `Name: ${this.name}, age: ${this.age}`;
        return p;
    }
}