import { Product } from "./Product";

export class Cart {
    /**
     * @type {Product[]}
     */
    products = [];
    /**
     * Ajoute un produit au panier
     * @param {Product} product 
     */
    addToCart(product) {
        this.products.push(product);
    }
    /**
     * Calcule le total des prix des produits
     * @returns {number}
     */
    getTotalPrice() {
        let sum = 0;
        for (const item of this.products) {
            sum+= item.price;
        }
        return sum;
    }
}